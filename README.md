Welcome to the [sourceflux](www.sourceflux.de) OpenFOAM Technology training repository

The repository contains both the simulation cases folder, as well as the source code for the solved exercises presented in the training sessions. As we have mentioned in the training sessions, the repository structure is similar to the structure of the main OpenFOAM/foam-extend platform.  

The applications have been compiled against the latest source version of OpenFOAM: 2.3.x. If you need us to revert to another platform version, please feel free to open an issue  on the repository website and we will take care of it as soon as possible. 

With time, we will integrate the solutions of all additional exercises into the repository. If you want us to make a specific additional exercise a priority, open an issue and let us know.  

Have fun working through the examples!

Jens & Tomislav


# Compiling applications and running exercise simulation cases

1. Make sure that the OpenFOAM installation is properly configured. If it has been installed by a regular user in $HOME, then execute 

    source ~/OpenFOAM/OpenFOAM-2.3.x/etc/bashrc

2. There are `Allwmake` and `Allclean` scripts prepared for automating the compilation of the training applications. Run `Allwmake` to compile the applications/solvers from the sourceflux OpenFOAM Technology Training slides. 


3. Every tutorial contains three scripts: 
    1. `Allrun`
        - Runs `blockMesh` if necessary. 
        - Runs `setFields` if necessary to set the initial field values.
        - Runs the solver. 
    2. `Allvisualize`
        - If applicable: opens a pre-saved ParaView state file for the visualization in a setup used during the training course.  
        - If applicable: analyzes the reported post-processing data and generates a PDF diagram report that you can view in your browser. 
    3. `Allclean`
        - Cleans the simulation case
        - Removes all log files.  
        - Removes all time step directories.
