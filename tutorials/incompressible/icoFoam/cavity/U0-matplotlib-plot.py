#!/usr/bin/python

from matplotlib import pyplot as plt
import numpy as np

plt.rc('text', usetex=True)

ghiaData = np.loadtxt('xprof.ghia')
pviewData = np.loadtxt('U0-line-sample-paraview.csv', skiprows=1)
sampleData = np.loadtxt('postProcessing/sets/25/lineX1_U.xy')

plt.plot(pviewData[:,7], pviewData[:,1], linewidth=4, color='black', label="icoFoam - ParaView sample")
plt.plot(sampleData[:,0], sampleData[:,1], linewidth=2, color='yellow', label="icoFoam - sample utility")
plt.plot(ghiaData[:,0], ghiaData[:,3], markersize=10, color='black', marker='x', linestyle='None',label="Ghia et al. 1982")
plt.tick_params(axis='both', which='major', labelsize=16)
plt.ylabel('$U_x \, \, [m/s]$', fontsize=20)
plt.xlabel('$y \, \, [m]$', fontsize=20)
plt.legend()
plt.savefig("U0-LDcavity-comparison.pdf")
