/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      controlDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

application     simpleFoam;

startFrom       latestTime;

startTime       0;

stopAt          endTime;

endTime         5000;

deltaT          1;

writeControl    timeStep;

writeInterval   100;

purgeWrite      0;

writeFormat     ascii;

writePrecision  6;

writeCompression off;

timeFormat      general;

timePrecision   6;

runTimeModifiable true;

functions {

    abortWhenConverged
    {
        functionObjectLibs ("libjobControl.so");
        type abort;
        action writeNow;
        fileName "converged";
    }

    testForConvergence
    {
        functionObjectLibs ("libutilityFunctionObjects.so");
        type coded;
        redirectType testForConvergence;
        outputControl outputTime;

        codeInclude
        #{
            #include "OFstream.H"
        #};

        code
        #{
        #};
        
        codeExecute
        #{
            const volVectorField& U =
                mesh().lookupObject<volVectorField>("U");

            volScalarField diffUmag = mag(U - U.oldTime());

            scalar diffUMax = max(diffUmag).value();

            // U.oldTime() at time 0 uses the same field values. Similar to BSD
            // o, oo at time 0. TM. 
            if (diffUMax > 0)
            {
                dimensionedScalar delta(
                    "small",
                    dimLength / dimTime,
                    SMALL
                );

                scalar relDiff = average(
                   diffUmag /
                   (mag(U.oldTime()) + delta)
                ).value();

                Info << "Convergence : " << relDiff << endl;

                // Uncomment cause a bug.
                // if (relDiff < 1e-03)
                if ((relDiff < 1e-03) && (relDiff > 0))
                {
                    OFstream convergenceFile("converged");
                }
            }

        #};
    }
}

// ************************************************************************* //
