#!/usr/bin/bash

pyFoamRunParameterVariation.py  --no-server-process \
                                --cloned-case-prefix="x" \
                                --every-variant-one-case-execution \
                                --create-database \
                                $1 $1.parameter
