/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::epicInterpolationScheme

Description

SourceFiles
    epicInterpolationSchemeI.H
    epicInterpolationScheme.C
    epicInterpolationSchemeIO.C

\*---------------------------------------------------------------------------*/

#ifndef epicInterpolationScheme_H
#define epicInterpolationScheme_H


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// Forward declaration of classes
class Ostream;

// Forward declaration of friend functions and operators
class epicInterpolationScheme;
Ostream& operator<<(Ostream&, const epicInterpolationScheme&);


/*---------------------------------------------------------------------------*\
                         Class epicInterpolationScheme Declaration
\*---------------------------------------------------------------------------*/

class epicInterpolationScheme
{

public:
        friend Ostream& operator<<(Ostream&, const epicInterpolationScheme&);
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
